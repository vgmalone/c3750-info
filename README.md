# c3750-info

When the  monitoring system alerts for a circuit or port flap on a Cisco 3750 L3 device, this script will pull the following information:
- show interface description
- show interface <port_number>
- Display system logs

## Prerequisites 
1. Python 3 installed on your workstation or laptop
2. Netmiko module installed
3. Your Cisco 3750 has been configured for SSH access, and it works.
  
    
## Useage  
After downloading the file, open it in your favorite editor and add your username and 'secret' password to the  
`device = ConnectHandler` section. Run the script as follows:  

> python `.\c3750-info.py` <device_ip_address> <port_number>  
> password:  

## Image
![The output from a properly running script!](command_prompt.PNG)


This script was created for, and tested, on a Cisco 3750 switch. I have no idea if it will work on a different device.