from netmiko import ConnectHandler
import getpass
import sys

devName = (sys.argv[1])
iFace = (sys.argv[2])

device = ConnectHandler(
    device_type='cisco_ios', 
    ip=devName,
    username='your username here',
    password=getpass.getpass(),
    secret='your secret password here'
)
device.enable()

def task1():
    # Get month and day
    output = device.send_command("show clock")
    tme = output.split(" ")
    dte = (tme[3] +" " + tme[4])
    print()

    # Get hostname
    output = device.send_command("show running-config | in hostname")
    h = output.split(" ")
    hostname = (h[1])

    # Collect general information on all ports       
    output = device.send_command("show int desc")
    print(hostname + "#show interface description")
    print(output)
    print()
    print()

    # Collection detailed info on a specific port
    output = device.send_command("show interface fastEthernet " + iFace)
    print(hostname + "#show interface fastEthernet " + iFace)
    print(output)
    print()
    print()

    # Collecting log files for incident
    output = device.send_command("show logging | include " + dte)
    print(hostname + "#show logging | include ", dte)
    print(output)
    print()
    print()    

task1()
device.disconnect()
